use strict;
use utf8;
use v5.10.0;
binmode STDOUT, ':encoding(UTF-8)';
use Data::Dumper;

open(my $pharm_katren, '<:encoding(UTF-8)', $ARGV[0]) or die $!;
open(my $pharm_366,    '<:encoding(UTF-8)', $ARGV[1]) or die $!;

my $data_katren = [];
my $data_366 = [];

while (my $row = <$pharm_katren>) {
	chomp $row;
	push @$data_katren, $row;
}

while (my $row = <$pharm_366>) {
	chomp $row;
	push @$data_366, $row;
}

my $matched = {};

for my $row ( @$data_katren ) {
	for my $row2 ( @$data_366 ) {
		$matched->{$row} += 1 if $row eq $row2;
	}
}

# print "$_: $matched->{$_}\n" for sort { $matched->{$b} <=> $matched->{$a} } keys %$matched;
my $c = 0;
for (keys %$matched) {
	print "$_\n" if $matched->{$_} == 1;
	$c++;
}
print "$c\n";