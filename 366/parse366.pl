use utf8;
use v5.10.0;
binmode STDOUT, ':encoding(UTF-8)';
use Data::Dumper;
use JSON::XS;

use constant {
    packing_form => {
        'ампулы'                    => 1,
        'аэрозоль'                  => 2,
        'гель'                      => 3,
        'гранулы'                   => 4,
        'драже'                     => 5,
        'капли'                     => 6,
        'капсулы'                   => 7,
        'концентрат'                => 8,
        'крем'                      => 9,
        'лак для ногтей'            => 10,
        'лиофилизат'                => 11,
        'мазь'                      => 12,
        'масло'                     => 13,
        'настойка'                  => 14,
        'пакеты'                    => 15,
        'пена'                      => 16,
        'порошок'                   => 17,
        'раствор'                   => 18,
        'сбор'                      => 19,
        'свечи'                     => 20,
        'сироп'                     => 21,
        'сок'                       => 22,
        'спрей'                     => 23,
        'суспензия'                 => 24,
        'таблетки'                  => 25,
        'таблетки для рассасывания' => 26,
        'шампунь'                   => 27,
        'экстракт'                  => 28,
        'эмульсия'                  => 29,
        'флакон'                    => 30,
        'NULL'                      => 31,
    },
    packing_count => {
        'count'  => 1, # штуки
        'gramms' => 2, # граммы
        'null'   => 3,
    },
    packing_volume => {
        'ml'   => 1,
        'null' => 2,
    },
    dose_count => {

    },
};

# my $words_hash = {};

# open(my $fh, '<:encoding(UTF-8)', $ARGV[0]) or die $!;

# while( my $row = <$fh> ) {
#     my @words = split('\s', $row);

#     for ( @words ) {
#         $words_hash->{$_} ||= 0;
#         $words_hash->{$_} += 1;
#     }
# }

# for ( sort { $words_hash->{$b} <=> $words_hash->{$a} } keys %$words_hash ) {
#     print sprintf("%s - %d\n", $_, $words_hash->{$_});
# }

my ( $raw_data,      $drugs,  $other ) = ([], [], []);
my ( $raw_data_hash, $dtypes, $dsyns ) = ({}, {}, {});

open(my $fh,        '<:encoding(UTF-8)', $ARGV[0]) or die $!;
open(my $dtypes_fh, '<:encoding(UTF-8)', $ARGV[1]) or die $!; # список форм выпуска
open(my $dsyns_fh,  '<:encoding(UTF-8)', $ARGV[2]) or die $!; # синонимы форм выпуска


while ( my $row = <$fh> ) {
    my ($uid, $manufacturer, $is_medication, $tmp_raw) = split(/\|/, $row);
    my $tmp_raw = join "", $manufacturer, $is_medication, $tmp_raw;

    $raw_data_hash->{$tmp_raw} = $row;
}
close $fh;

$raw_data = [ values %$raw_data_hash ];

while ( my $type = <$dtypes_fh> ) {
    chomp $type;
    $type = uc $type;
    $dtypes->{$type} = $type;
}
close $dtypes_fh;

while ( my $row = <$dsyns_fh> ) {
    chomp $row;
    my ( $syn, $orig ) = split ':', $row;

    $dsyns->{$syn} = $orig;
}
close $dsyns_fh;


for my $raw ( @$raw_data ) {

    my ($uid, $manufacturer, $is_medication);

    ($uid, $manufacturer, $is_medication, $raw) = split(/\|/, $raw);

    $raw =~ s|\.([№\dN])|\. $1|g;
    $raw =~ s|\\|/|g;
    $raw =~ s|'||g;

    my @title = split /\s/, $raw;

    chomp $raw;

    my $drug = {
        packing_form    => [], # типов может быть несколько
        uid             => $uid,
        raw             => $raw,
        title           => [],
        dose            => undef,
        dose_form       => undef,
        volume          => undef,
        volume_form     => undef,
        count           => undef,
        count_form      => undef,
        cutted          => $raw,
        active          => 0,
        is_prescription => 0,
        manufacturer    => $manufacturer || 'NULL',
        is_medication   => $is_medication,
    };

    for $name_part ( @title ) {
        if ( exists $dtypes->{$name_part} || exists $dsyns->{$name_part} ) {
            my $tmp_type = $dtypes->{$name_part} || $dsyns->{$name_part};

            # $drug->{cutted} =~ s/\Q$name_part\E//i;

            push @{$drug->{packing_form}}, $tmp_type;
            last;
        }

        push @{$drug->{title}}, $name_part;
    }

    $drug->{title} = ucfirst lc join(" ", @{$drug->{title}});
    chomp $drug->{title};
    $drug->{title} =~ s/[\s\t]*$//;

    my $name = $drug->{title};
    $drug->{cutted} =~ s/\Q$name\E//i;

    $drug->{dose}  = _find_dose($raw);
    $drug->{count} = _find_volume($raw);

    chomp $drug->{dose};
    chomp $drug->{count};


    $drug->{dose}   ||= 'NULL';
    $drug->{count}  ||= 'NULL';
    $drug->{volume} ||= 'NULL';

    $drug->{dose} = 'NULL' if ( $drug->{dose} eq $drug->{count} );


    if ( scalar grep { lc($_) eq 'флакон' } @{$drug->{packing_form}} ) {
        $drug->{packing_form} = 'флакон';
    }
    else {
        $drug->{packing_form} = [sort {$a cmp $b } @{$drug->{packing_form}}];
        $drug->{packing_form} = lc($drug->{packing_form}->[0]) || 'NULL';
    }
    $drug->{packing_form} = packing_form->{$drug->{packing_form}};


    # удаляем дозировки и количества из заголовка
    my $dose = $drug->{dose};
    my $count = $drug->{count};
    $drug->{title} =~ s/\Q$dose\E//i;
    $drug->{title} =~ s/\Q$count\E//i;

    $drug->{cutted} =~ s/\Q$dose\E//i;
    $drug->{cutted} =~ s/\Q$count\E//i;

    $drug->{count} =~ s/^\s*//;
    $drug->{count} =~ s/\s*$//;

    $drug->{volume_form} = packing_volume->{null};


    if ( $drug->{count} =~ /^(?:N|№)\s*([\d,]*)$/i ) {
        $drug->{count} = $1;
        $drug->{count_form} = packing_count->{count};
    }
    elsif ( $drug->{count} =~ /^([\d,]*)\s*МЛ$/i ) {
        $drug->{volume} = $1;
        $drug->{volume_form} = packing_volume->{ml};
        $drug->{count} = 'NULL';
    }
    elsif ( $drug->{count} =~ /([\d,]*)\s*МЛ (?:N|№)([\d,]*)\s*$/i ) {
        $drug->{volume} = $1;
        $drug->{count}  = $2;

        $drug->{volume_form} = packing_volume->{ml};
        $drug->{count_form}  = packing_count->{count};
    }
    elsif ( $drug->{count} =~ /^([\d,]*)\s*(г|гр)$/i ) {
        $drug->{count} = $1;
        $drug->{count_form} = packing_count->{gramms};
    }

    $drug->{count}      ||= 0;
    $drug->{count_form} ||= packing_count->{null};


    if ( $drug->{dose} eq 'NULL' ) {
        $drug->{dose_form} = 'NULL';
    }
    elsif ( $drug->{dose} =~ /^([\d,]*)\s*мг$/i ) {
        $drug->{dose_form} = 'мг';
        $drug->{dose} = $1;
    }
    elsif ( $drug->{dose} =~ /^([\d,]*)\s*г$/i ) {
        $drug->{dose_form} = 'гр';
        $drug->{dose} = $1;
    }
    elsif ( $drug->{dose} =~ /^([\d,]*)\s*%$/i ) {
        $drug->{dose_form} = '%';
        $drug->{dose} = $1;
    }
    elsif ( $drug->{dose} =~ /^([\d,]*)\s*ме$/i ) {
        $drug->{dose_form} = 'ме';
        $drug->{dose} = $1;
    }
    elsif ( $drug->{dose} =~ /^([\d,]*)\s*мкг$/i ) {
        $drug->{dose_form} = 'мкг';
        $drug->{dose} = $1;
    }
    elsif ( $drug->{dose} =~ /^([\d,]*)\s*ед$/i ) {
        $drug->{dose_form} = 'ед';
        $drug->{dose} = $1;
    }
    elsif ( $drug->{dose} =~ /^([\d,]*)\s*доз$/i ) {
        $drug->{dose_form} = 'доз';
        $drug->{dose} = $1;
    }
    elsif ( $drug->{dose} =~ /^([\d,]+) \+ ([\d,]+)$/ix ) {
        $drug->{dose} = "$1 + $2";
        $drug->{dose_form} = 'NULL';
    }
    elsif ( $drug->{dose} =~ /^([\d,]+) \+ ([\d,]+) ([^\d\s,]+) $/ix ) {
        $drug->{dose} = "$1 + $2";
        $drug->{dose_form} = $3;
    }
    elsif ( $drug->{dose} =~ /^([\d,]+) ([^\d\s,]+) \+ ([\d,]+) ([^\d\s,]+)$/ix ) {
        $drug->{dose} = "$1 + $3";
        $drug->{dose_form} = "$2 + $4";
    }
    elsif ( $drug->{dose} =~ /^([\d,]+)([a-zа-я]+\/[a-zа-я]+)$/ix ) {
        $drug->{dose} = $1;
        $drug->{dose_form} = $2;
    }

    $drug->{dose_form} ||= 'NULL';

    # вырезаем окончания(потенциально лишняя часть)
    if ( $drug->{title} =~ /\s{2,}(.*)$/ ) {
        $drug->{title} =~ s/\s{2,}.*$//;
    }

    $drug->{title} =~ s/^\s*//;
    $drug->{title} =~ s/\s*$//;
    $drug->{title} = ucfirst lc $drug->{title};

    $drug->{cutted} =~ s/^\s*//;
    $drug->{cutted} =~ s/\s*$//;

    push @$drugs, $drug;
}


my $manufacturer_h = {};
for my $d ( @$drugs ) {
    $manufacturer_h->{$d->{manufacturer}} = 1;
}

# for my $k ( keys %$manufacturer_h ) {
#     print sprintf("INSERT INTO provider_manufacturer(title) VALUES ('%s');\n", $k);
# }

my ( $typed, $untyped ) = (0, 0);

$format = "%s\t%d\t%s\t%s\t%s\t%d\t%s\t%d\t%d\t%s\t%s\t%d\n";

# print sprintf("title\ttype\tdose\tdose_form\tvolume\tvolume_form\tcount\tcount_form\tactive\traw\tcutted\tid\tmanf\n");


# $format = "\t\t\tINSERT INTO provider_position(buy_count, is_prescription, title, packing_form, dose, dose_form, volume, volume_form, count, count_form, active, raw, cutted, manufacturer_id) SELECT 0, 0, %s, %d, %s, %s, %s, %d, %d, %d, %d, %s, %s, pm.id FROM provider_manufacturer pm WHERE pm.title = %s LIMIT 1;\n";

for my $drug ( @$drugs ) {
# print Dumper $drug;
    # print join('#', $drug->{name}, $drug->{type}, $drug->{count}, $drug->{count_form}, $drug->{dose}, $drug->{dose_form}, $drug->{volume}, $drug->{volume_form}, "\n");

    $drug->{active} = 1 if ($drug->{dose} ne 'NULL') || ($drug->{volume} ne 'NULL') || ($drug->{count} ne 'NULL');

    # next unless $drug->{active};

    for (qw/ title dose dose_form volume raw cutted manufacturer/) {
        if ( ($drug->{$_} eq '') || ($drug->{$_} eq 'NULL') ) {
            $drug->{$_} = "NULL";
        }
        else {
            # $drug->{$_} = "\'$drug->{$_}\'";
        }
    }

    $drug->{manufacturer} = "''" if $drug->{manufacturer} eq 'NULL';

    if ( $drug->{title} eq 'NULL' ) {
        $drug->{title} = $drug->{raw};
        $drug->{active} = 0;
    }

    # print sprintf($format, $drug->{title}, $drug->{packing_form}, $drug->{dose}, $drug->{dose_form}, $drug->{volume}, $drug->{volume_form}, $drug->{count}, $drug->{count_form}, $drug->{active}, $drug->{raw}, $drug->{cutted}, $drug->{manufacturer});

    my $json = JSON::XS->new->encode({
        active          => ($drug->{active}          eq 'NULL' ? (undef) : (int $drug->{active}           ) ),
        buy_count       => 0,
        count           => ($drug->{count}           eq 'NULL' ? (undef) : (int $drug->{count}            ) ),
        count_form      => ($drug->{count_form}      eq 'NULL' ? (undef) : (int $drug->{count_form}       ) ),
        cutted          => ($drug->{cutted}          eq 'NULL' ? (undef) : (    $drug->{cutted}           ) ),
        dose            => ($drug->{dose}            eq 'NULL' ? (undef) : (    $drug->{dose}             ) ),
        dose_form       => ($drug->{dose_form}       eq 'NULL' ? (undef) : (    $drug->{dose_form}        ) ),
        is_medication   => ($drug->{is_medication}   eq 'NULL' ? (undef) : (int $drug->{is_medication}    ) ),
        is_prescription => ($drug->{is_prescription} eq 'NULL' ? (undef) : (int $drug->{is_prescription}  ) ),
        manufacturer    => ($drug->{manufacturer}    eq 'NULL' ? (undef) : (    $drug->{manufacturer}     ) ),
        packing_form    => ($drug->{packing_form}    eq 'NULL' ? (undef) : (int $drug->{packing_form}     ) ),
        raw             => ($drug->{raw}             eq 'NULL' ? (undef) : (    $drug->{raw}              ) ),
        title           => ($drug->{title}           eq 'NULL' ? (undef) : (    $drug->{title}            ) ),
        volume          => ($drug->{volume}          eq 'NULL' ? (undef) : (    $drug->{volume}           ) ),
        volume_form     => ($drug->{volume_form}     eq 'NULL' ? (undef) : (int $drug->{volume_form}      ) ),
    });


    print sprintf("\t\t\tINSERT INTO provider_providerposition(title, external_uid, content, processed) SELECT ('%s', '%s', '%s', 0) ON DUPLICATE KEY UPDATE title = VALUES(title), content = VALUES(content);\n", $drug->{raw}, $drug->{uid}, $json);


    # $format = "\t\t\tUPDATE provider_providerposition SET content = '%s' WHERE uid = '%s'\n";
    # print sprintf($format, $json, $drug->{uid});

}


# print sprintf("typed: %d\nuntyped: %d\n", $typed, $untyped);


sub _find_volume {
    my ( $row ) = @_;

    my ( $gr     ) = ( $row =~ m/([\d,]+? \s* (?:г|гр) ) (?:\s|$) /ix );
    my ( $nx     ) = ( $row =~ m/(N \d+)                 (?:\s|$) /ix );
    my ( $ml     ) = ( $row =~ m/([\d,]+? \s* МЛ)        (?:\s|$) /ix );
    my ( $number ) = ( $row =~ m/(№ \d+)                 (?:\s|$) /ix );

    my $result;

    $result = sprintf ("%s %s", $ml,   $nx)     if $nx     && $ml;
    $result = sprintf ("%s %s", $ml,   $number) if $number && $ml;

    $result ||= $nx || $ml || $number || $gr;

    chomp $result;

    return $result || 'NULL';
}


sub _find_dose {
    my ( $row ) = @_;

    my $result;

    $result = ( $row =~ m/([\d,]+? \s* г                          ) (?:\s|$) /ix ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+? \s* %                          ) (?:\s|$) /ix ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+? \s* МГ                         ) (?:\s|$) /ix ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+? \/ МЛ                          ) (?:\s|$) /ix ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+? \s* МКГ                        ) (?:\s|$) /ix ) ? $1 : $result;
    $result = ( $row =~ m/(\d+?    \s* ЕД                         ) (?:\s|$) /ix ) ? $1 : $result;
    $result = ( $row =~ m/(\d+?    \s* МЕ                         ) (?:\s|$) /ix ) ? $1 : $result;
    $result = ( $row =~ m/(\d+?    \s* МЛН\sЕД                    ) (?:\s|$) /ix ) ? $1 : $result;
    $result = ( $row =~ m/(\d+?    \s* (ДОЗA|ДОЗ)                 ) (?:\s|$) /ix ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+? \s* МЕ\/Г                      ) (?:\s|$) /ix ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+? \s* МГ\/Г                      ) (?:\s|$) /ix ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+? \s* МКГ\/Г                     ) (?:\s|$) /ix ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+? \s* ЕД\/Г                      ) (?:\s|$) /ix ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+? \s* МГ\/Д                      ) (?:\s|$) /ix ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+? \s* МГ\/ДОЗА                   ) (?:\s|$) /ix ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+? \s* МКГ\/Д                     ) (?:\s|$) /ix ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+? \s* МКГ\/ДОЗА                  ) (?:\s|$) /ix ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+? \+ [\d,]+?                     ) (?:\s|$) /ix ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+?[^\d\s,]*? \+ [\d,]+?[^\d\s,]*? ) (?:\s|$) /ix ) ? $1 : $result;

    chomp $result;

    return $result || 'NULL';
}
