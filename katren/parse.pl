use utf8;
use v5.10.0;
binmode STDOUT, ':encoding(UTF-8)';
use Data::Dumper;
use JSON::XS;

use constant {
    packing_form => {
        'ампулы'                    => 1,
        'аэрозоль'                  => 2,
        'гель'                      => 3,
        'гранулы'                   => 4,
        'драже'                     => 5,
        'капли'                     => 6,
        'капсулы'                   => 7,
        'концентрат'                => 8,
        'крем'                      => 9,
        'лак для ногтей'            => 10,
        'лиофилизат'                => 11,
        'мазь'                      => 12,
        'масло'                     => 13,
        'настойка'                  => 14,
        'пакеты'                    => 15,
        'пена'                      => 16,
        'порошок'                   => 17,
        'раствор'                   => 18,
        'сбор'                      => 19,
        'свечи'                     => 20,
        'сироп'                     => 21,
        'сок'                       => 22,
        'спрей'                     => 23,
        'суспензия'                 => 24,
        'таблетки'                  => 25,
        'таблетки для рассасывания' => 26,
        'шампунь'                   => 27,
        'экстракт'                  => 28,
        'эмульсия'                  => 29,
        'флакон'                    => 30,
        'NULL'                      => 31,
    },
    packing_count => {
        'count'  => 1, # штуки
        'gramms' => 2, # граммы
        'null'   => 3,
    },
    packing_volume => {
        'ml'   => 1,
        'null' => 2,
    },
    dose_count => {

    },
};


my ( $raw_data,      $drugs, $other  ) = ([], [], []);
my ( $raw_data_hash, $dtypes, $dsyns ) = ({}, {}, {});


# my $format = "%s\t%d\t%s\t%s\t%s\t%d\t%s\t%d\t%d\t%s\t%s\t%d\n";
# my $format = "UPDATE provider_position SET title = '%s', packing_form = %d, dose = '%s', dose_form = '%s', volume = '%s', volume_form = %d, count = %d, count_form = %d, active = %d, raw = '%s', cutted = '%s' WHERE id = %d;\n";

open(my $fh,        '<:encoding(UTF-8)', $ARGV[0]) or die $!; # сырые данные
open(my $dtypes_fh, '<:encoding(UTF-8)', $ARGV[1]) or die $!; # список форм выпуска
open(my $dsyns_fh,  '<:encoding(UTF-8)', $ARGV[2]) or die $!; # синонимы форм выпуска

while ( my $row = <$fh> ) {
    my ($id, $title) = split '---', $row;
    $raw_data_hash->{$title} = $row;
}
close $fh;

$raw_data = [ values %$raw_data_hash ];

while ( my $type = <$dtypes_fh> ) {
    chomp $type;
    $type = uc $type;
    $dtypes->{$type} = 1;
}
close $dtypes_fh;

while ( my $row = <$dsyns_fh> ) {
    chomp $row;
    my ( $syn, $orig ) = split ':', $row;

    $dsyns->{$syn} = $orig;
}
close $dsyns_fh;



for my $row ( @$raw_data ) {
    chomp $row;
    $row =~ s/[\s\t\n]*$//;

    my ($uid, $title) = split '---', $row;

    $title =~ s|\\|/|g;
    $title =~ s|'||g;

    my $drug = {
        active          => 0,
        count           => undef,
        count_form      => undef,
        cutted          => $raw,
        dose            => undef,
        dose_form       => undef,
        is_medication   => 0,
        is_prescription => 0,
        manufacturer    => 'NULL',
        name            => undef,
        raw             => $raw,
        raw             => $title,
        type            => [], # типов может быть несколько
        uid             => $uid,
        volume          => undef,
        volume_form     => undef,
    };

    # название обычно заканчивается с первой цифрой или буквой N
    # ( $drug->{name} ) = ( $title =~ m/^(.*?) \s (N|([\d,]+?\s)) /xi );
    # $drug->{name} = 'NULL' unless length $drug->{name};
    $drug->{name} = $title;

    chomp $drug->{name};
    $drug->{name} =~ s/[\s\t\n]*$//;



    # сначала форма выпуска по синонимам типов
    for my $syn ( keys %$dsyns ) {
        push @{$drug->{type}}, $dsyns->{$syn} if ( $title =~ m|\b$syn\b| );
    }

    # потом по типам
    for my $type ( keys %$dtypes ) {
        push @{$drug->{type}}, $type if ( $title =~ m|\b$type\b| );
    }

    $drug->{dose}  = _find_dose($row);
    $drug->{count} = _find_volume($row);

    chomp $drug->{dose};
    chomp $drug->{count};

    $drug->{dose}   ||= 'NULL';
    $drug->{count}  ||= 'NULL';
    $drug->{volume} ||= 'NULL';

    $drug->{dose} = 'NULL' if ( $drug->{count} eq $drug->{dose} );

    # если есть флакон, то нужно это выделить, т.к внутри него могут быть разные формы выпуска
    if ( scalar grep { lc($_) eq 'флакон' } @{$drug->{type}} ) {
        $drug->{type} = 'флакон';
    }
    else {
        $drug->{type} = [sort {$a cmp $b } @{$drug->{type}}];
        $drug->{type} = lc($drug->{type}->[0]) || 'NULL';
    }
    $drug->{type} = packing_form->{$drug->{type}};

    # удаляем дозировки и количества из заголовка
    my $dose = $drug->{dose};
    my $count = $drug->{count};
    $drug->{name} =~ s/\Q$dose\E//i;
    $drug->{name} =~ s/\Q$count\E//i;

    # разделяем объём и штуки

    ## при связке xМЛ yN и xМЛ
    if ( $drug->{count} =~ m/([\d,]+?\s*МЛ \s N\d+)$/xi ) {
        ($drug->{volume}, $drug->{count}) = ($drug->{count} =~ m/([\d,]+?\s*МЛ) \s (N\d+)$/xi);
    }
    elsif ( $drug->{count} =~ m/МЛ/ ) {
        $drug->{volume} = $drug->{count};
        $drug->{count} = 'NULL';
    }

    if ($drug->{count} =~ /^N[\d]+$/) {
        $drug->{count_form} = packing_count->{count};
        $drug->{count} =~ s/N//;
    }
    elsif ($drug->{count} =~ /^[\d,]+$/) {
        $drug->{count_form} = packing_count->{gramms};
    }
    else {
        $drug->{count_form} = packing_count->{null};
    }

    if ($drug->{volume} =~ /МЛ/) {
        $drug->{volume_form} = packing_volume->{ml};
        $drug->{volume} =~ s/МЛ//;
    }
    else {
        $drug->{volume_form} = packing_volume->{null};
    }

    $drug->{dose} =~ s/\s*$//;
    $drug->{dose} =~ s/^\s*//;


    if ( $drug->{dose} =~ /^[\d,]+$/ ) {
        ($drug->{dose}, $drug->{dose_form}) = _get_mass_unit($drug->{dose});
    }
    elsif ( $drug->{dose} =~ /^[\d,]+%$/ ) {
        $drug->{dose_form} = '%';
        $drug->{dose} =~ s/%//;
    }
    elsif ( $drug->{dose} =~ /^([\d,]+)\/МЛ$/ ) {
        $drug->{dose} = $1;
        ($drug->{dose}, $drug->{dose_form}) = _get_mass_unit($drug->{dose});
        $drug->{dose_form} .= '/мл';
    }
    elsif ( $drug->{dose} eq 'NULL' ) {
        $drug->{dose_form} = 'NULL';
    }
    elsif ( $drug->{dose} =~ /^([\d,]+)\s*ЕД$/ ) {
        $drug->{dose_form} = 'ед';
        $drug->{dose} = $1;
    }
    elsif ( $drug->{dose} =~ /^([\d,]+)\s*КЕД$/ ) {
        $drug->{dose_form} = 'кед';
        $drug->{dose} = $1;
    }
    elsif ( $drug->{dose} =~ /^([\d,]+)\+([\d,]+)$/ ) {
        my ($var1, $unit1) = _get_mass_unit($1);
        my ($var2, $unit2) = _get_mass_unit($2);

        $drug->{dose}      = "$var1 + $var2";
        $drug->{dose_form} = "$unit1 + $unit2";
    }
    elsif ( $drug->{dose} =~ /^([\d,]+)\s*МЕ$/ ) {
        $drug->{dose} = $1;
        $drug->{dose_form} = 'ме';
    }

    if ( (! exists $drug->{dose_form}) || ($drug->{dose_form} eq '') || (! defined $drug->{dose_form}) ) {
        $drug->{dose_form} = 'NULL';
    }


    # удаляем форму выпуска из заголовка
    for my $syn ( keys %$dsyns ) {
        if ( $drug->{name} =~ m|\b$syn\b| ) {
            $drug->{name} =~ s/$syn//;
        }
    }
    for my $type ( keys %$dtypes ) {
        if ( $drug->{name} =~ m|\b$type\b| ) {
            $drug->{name} =~ s/$type//;
        }
    }

    # вырезаем окончания(потенциально лишняя часть)
    if ( $drug->{name} =~ /\s{2,}(.*)$/ ) {
        $drug->{cutted} = $1;
        $drug->{name} =~ s/\s{2,}.*$//;
    }

    $drug->{name} =~ s/^\s*//;
    $drug->{name} =~ s/\s*$//;
    $drug->{name} = ucfirst lc $drug->{name};

    push @$drugs, $drug;
}

# print "name\ttype\tdose\tdose_form\tvolume\tvolume_form\tcount\tcount_form\tis_visible\traw\tcutted\tuid\n";

my ($typed, $untyped) = (0, 0);

for my $drug ( @$drugs ) {

    $drug->{active} = 1;

    $drug->{active} &&= 0 if ( $drug->{name} eq $drug->{raw} );
    $drug->{active} &&= 0 if ( $drug->{dose_form} eq 'gr' && $drug->{dose} >= 5 );
    $drug->{active} &&= 0 if ( $drug->{dose} eq 'NULL' && $drug->{volume} eq 'NULL' && $drug->{count} eq 'NULL' );

    $drug->{type} ||= 'NULL';
    $drug->{name} = ucfirst lc $drug->{name};

    # print join('#', $drug->{name}, $drug->{type}, $drug->{count}, $drug->{count_form}, $drug->{dose}, $drug->{dose_form}, $drug->{volume}, $drug->{volume_form}, "\n");

    for (qw/ name dose dose_form volume raw cutted manufacturer/) {
        if ( ($drug->{$_} eq '') || ($drug->{$_} eq 'NULL') ) {
            $drug->{$_} = "NULL";
        }
        else {
            # $drug->{$_} = "\'$drug->{$_}\'";
        }
    }

    $drug->{manufacturer} = "''" if $drug->{manufacturer} eq 'NULL';

    if ( $drug->{name} eq 'NULL' ) {
        $drug->{name} = $drug->{raw};
        $drug->{active} = 0;
    }

    # print join "\t",
    #     $drug->{name},
    #     $drug->{type},
    #     $drug->{dose},
    #     $drug->{dose_form},
    #     $drug->{volume},
    #     $drug->{volume_form},
    #     $drug->{count},
    #     $drug->{count_form},
    #     $drug->{active},
    #     $drug->{raw},
    #     $drug->{cutted},
    #     $drug->{uid},
    # ;
    # print "\n";


    # print sprintf($format, $drug->{title}, $drug->{packing_form}, $drug->{dose}, $drug->{dose_form}, $drug->{volume}, $drug->{volume_form}, $drug->{count}, $drug->{count_form}, $drug->{active}, $drug->{raw}, $drug->{cutted}, $drug->{manufacturer});

    my $json = JSON::XS->new->encode({
        active          => ($drug->{active}          eq 'NULL' ? (undef) : (int $drug->{active}           ) ),
        buy_count       => 0,
        count           => ($drug->{count}           eq 'NULL' ? (undef) : (int $drug->{count}            ) ),
        count_form      => ($drug->{count_form}      eq 'NULL' ? (undef) : (int $drug->{count_form}       ) ),
        cutted          => ($drug->{cutted}          eq 'NULL' ? (undef) : (    $drug->{cutted}           ) ),
        dose            => ($drug->{dose}            eq 'NULL' ? (undef) : (    $drug->{dose}             ) ),
        dose_form       => ($drug->{dose_form}       eq 'NULL' ? (undef) : (    $drug->{dose_form}        ) ),
        is_medication   => ($drug->{is_medication}   eq 'NULL' ? (undef) : (int $drug->{is_medication}    ) ),
        is_prescription => ($drug->{is_prescription} eq 'NULL' ? (undef) : (int $drug->{is_prescription}  ) ),
        manufacturer    => ($drug->{manufacturer}    eq 'NULL' ? (undef) : (    $drug->{manufacturer}     ) ),
        packing_form    => ($drug->{packing_form}    eq 'NULL' ? (undef) : (int $drug->{type}             ) ), #type
        raw             => ($drug->{raw}             eq 'NULL' ? (undef) : (    $drug->{raw}              ) ),
        title           => ($drug->{name}            eq 'NULL' ? (undef) : (    $drug->{name}             ) ), #name
        volume          => ($drug->{volume}          eq 'NULL' ? (undef) : (    $drug->{volume}           ) ),
        volume_form     => ($drug->{volume_form}     eq 'NULL' ? (undef) : (int $drug->{volume_form}      ) ),
    });


    $format = "\t\t\tUPDATE provider_providerposition SET content = '%s' WHERE uid = '%s';\n";
    print sprintf($format, $json, $drug->{uid});

}


# print sprintf("typed: %d\nuntyped: %d\n", $typed, $untyped);

sub _find_volume {
    my ( $row ) = @_;

    my ( $x    ) = ( $row =~ m/\s ([\d,]+?)        (?:\s|$) /xi );
    my ( $nx   ) = ( $row =~ m/   (N\d+?)          (?:\s|$) /xi );
    my ( $ml   ) = ( $row =~ m/\s ([\d,]+? \s* МЛ) (?:\s|$) /xi );
    my ( $nxx  ) = ( $row =~ m/   (N\d+? X \d+?)   (?:\s|$) /xi );

    if ( $nxx ) {
        ($x, $y) = ( $row =~ m/N(\d+)? X (\d+)?/xi );
        $nxx = $x * $y;
        $nxx = "N$nxx";
    }

    return sprintf ("%s %s", $ml, $nx) if $nx && $ml;

    return $nx || $ml || $nxx || $x;
}


sub _find_dose {
    my ( $row ) = @_;

    my $result;

    $result = ( $row =~ m/\s ([\d,]+?                         ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/\s (([\d,]+?) \+ ([\d,]+?)          ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/\s (([\d,]+?) \/ ([\d,]+?)          ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+?   \s* г                    ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+?   \s* %                    ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+?   \/ МЛ                    ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+?   \s* ЕД                   ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+?   \s* ДОЗ                  ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+?   \s* МЕ                   ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+?   \s* МГ\/МЛ               ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+?   \s* МКГ\/МЛ              ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+?   \s* МЕ\/МЛ               ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+?   \s* МЕ\/Г                ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+?   \s* ЕД\/МЛ               ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+?   \s* ЕД\/ГР               ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/\s([\d,]+? \s* МГ\/ДОЗА             ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/\s([\d,]+? \s* МКГ\/ДОЗА            ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/\s([\d,]+? \s* МКГ                  ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/\s([\d,]+? \s* МГ                   ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/\s([\d,]+? \s* КЕ                   ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/\s([\d,]+? \s* КЕД                  ) (?:\s|$) /xi ) ? $1 : $result;

    $result = ( $row =~ m/([\d,]+? \/ [\d,]+? \s* МЛ          ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+? \+ [\d,]+? \s* МЛ          ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/([\d,]+? \+ [\d,]+? \/ [\d,] МЛ     ) (?:\s|$) /xi ) ? $1 : $result;

    $result = ( $row =~ m/(([\d,]+?)МГ  \+ ([\d,]+?)МГ\/ДОЗА  ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/(([\d,]+?)МКГ \+ ([\d,]+?)МКГ\/ДОЗА ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/(([\d,]+?)МГ  \+ ([\d,]+?)МГ        ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/(([\d,]+?)МГ  \+ ([\d,]+?)МЛ        ) (?:\s|$) /xi ) ? $1 : $result;
    $result = ( $row =~ m/(([\d,]+?)МКГ \+ ([\d,]+?)МКГ       ) (?:\s|$) /xi ) ? $1 : $result;

    return $result;
}


sub _get_mass_unit {
    my ( $value ) = @_;

    $value =~ s/,/\./;

    my $unit = 'гр';

    for (qw/ мг мкг /) {
        last if $value >= 1;

        $unit = $_;

        $value *= 1000;
    }

    $value =~ s/\./,/;

    return wantarray ? ($value, $unit) : $unit;
}